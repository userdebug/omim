package com.mapswithme.maps.purchase;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Collections;
import java.util.List;

class SubsProductDetailsCallback
    extends StatefulPurchaseCallback<BookmarkPaymentState, BookmarkPaymentFragment>
    implements PlayStoreBillingCallback
{
  @Override
  void onAttach(@NonNull BookmarkPaymentFragment bookmarkPaymentFragment)
  {
  }
}
