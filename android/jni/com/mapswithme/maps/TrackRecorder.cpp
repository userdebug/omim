#include "Framework.hpp"

#include "com/mapswithme/core/jni_helper.hpp"
#include "map/gps_tracker.hpp"

#include <chrono>

namespace
{

::Framework * frm()
{
  return (g_framework ? g_framework->NativeFramework() : nullptr);
}

}  // namespace

extern "C"
{
  JNIEXPORT void JNICALL
  Java_com_mapswithme_maps_location_TrackRecorder_nativeSetEnabled(JNIEnv * env, jclass clazz, jboolean enable)
  {
    GpsTracker::Instance().SetEnabled(enable);
    Framework * const f = frm();
    if (f == nullptr)
      return;
    if (enable)
      f->ConnectToGpsTracker();
    else
      f->DisconnectFromGpsTracker();
  }

  JNIEXPORT jboolean JNICALL
  Java_com_mapswithme_maps_location_TrackRecorder_nativeIsEnabled(JNIEnv * env, jclass clazz)
  {
    return GpsTracker::Instance().IsEnabled();
  }

  JNIEXPORT void JNICALL
  Java_com_mapswithme_maps_location_TrackRecorder_nativeSetDuration(JNIEnv * env, jclass clazz, jint durationHours)
  {
    GpsTracker::Instance().SetDuration(std::chrono::hours(durationHours));
  }

  JNIEXPORT jint JNICALL
  Java_com_mapswithme_maps_location_TrackRecorder_nativeGetDuration(JNIEnv * env, jclass clazz)
  {
    return GpsTracker::Instance().GetDuration().count();
  }

  JNIEXPORT void JNICALL
  Java_com_mapswithme_maps_location_TrackRecorder_nativeSaveTrack(JNIEnv * env, jclass clazz, jstring name)
  {
    ::Framework *f = frm();

    std::vector<location::GpsInfo> toAdd;
    GpsTracker::Instance().GetTrack()->GetPoints(toAdd);
    if(toAdd.empty())
      return;

    std::vector<location::GpsTrackInfo> outPoints;

    GpsTrackFilter filter;
    filter.Process(toAdd, outPoints);

    kml::TrackData data;

    for (auto const & ip : outPoints)
    {
      m2::PointD pt(MercatorBounds::FromLatLon(ip.m_latitude, ip.m_longitude));
      if(pt.IsAlmostZero())
        continue;
      data.m_points.push_back(pt);
    }
    if(data.m_points.size() < 2) {
        GpsTracker::Instance().GetTrack()->Clear();
        f->GetDrapeEngine()->ClearGpsTrackPoints();
        return;
    }

    kml::TrackLayer layer;
    layer.m_color.m_rgba = 0x0000ffff; // dp::Color(0, 0, 255, 255)
    data.m_layers.push_back(layer);
    kml::SetDefaultStr(data.m_name, jni::ToNativeString(env, name));

    Track const *t = f->CreateTrack(std::move(data));

    GpsTracker::Instance().GetTrack()->Clear();
    f->GetDrapeEngine()->ClearGpsTrackPoints();

    // f->ShowTrack(*t);
  }

  JNIEXPORT void JNICALL
  Java_com_mapswithme_maps_location_TrackRecorder_nativeClearTrack(JNIEnv * env, jclass clazz)
  {
    GpsTracker::Instance().GetTrack()->Clear();
    frm()->GetDrapeEngine()->ClearGpsTrackPoints();
  }
}
